package com.telkomsel.mytelkomsel.core.modules;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ModuleFactory {
    private ModuleFactory() { }

    /**
     * generate the meta description mapping from json file
     * @param context
     * @return
     */
    public static Map<String, IModuleItemMeta> createModuleItemMeta(Context context) {
        Map<String, IModuleItemMeta> metaDataList = new HashMap<>();

        String configJson = ModuleUtils.readFile(context, "modules/moduleMeta.json");
        Type type = new TypeToken<List<ModuleManager.ModuleItemMeta>>(){}.getType();
        List<ModuleManager.ModuleItemMeta> configMetaDataList = ModuleUtils.fromJson(configJson, type);
        printLog("createModuleItemMeta : " + configMetaDataList.size());
        for(ModuleManager.ModuleItemMeta metaData : configMetaDataList) {
            printLog("metaData : " + metaData);
            metaDataList.put(metaData.getId(), metaData);
        }
        return metaDataList;
    }

    public static List<Class<?>> createModuleClasses(Context context) {
        printLog("createModuleClasses");
		List<Class<?>> moduleClasses = new ArrayList<>();
        try {
			String configJson = ModuleUtils.readFile(context, "modules/moduleClasses.json");
			JsonArray arr = JsonParser.parseString(configJson).getAsJsonArray();
			if (arr.size() <= 0) {
				throw new Exception("No module to load !!!");
			}

			for(JsonElement e : arr) {
				String className = e.getAsString();
				printLog("load class '" + className + "'");
				Class cls = null;
				try {
					cls = Class.forName(className);
					moduleClasses.add(cls);
					printLog("class '" + className + "' successfully loaded...");
				} catch(Exception exc) {
					printLog("Exception caught on createModuleClasses.load : " + exc);
					continue;
				}
			}
        } catch(Exception exc) {
            exc.printStackTrace();
            printLog("Exception caught on createModuleClasses : " + exc);
        }

		return moduleClasses;
    }

    /**
     * create ModuleItemDescriptor based on the mapping meta description
     */
    public static Map<String, IModuleItemDescriptor> createModuleItemDescriptor(Map<String, IModuleItemMeta> metaDataMap) {
        printLog("createModuleItemDescriptor : " + metaDataMap.size());
        Map<String, IModuleItemDescriptor> descriptors = new HashMap<>();
        for(String id : metaDataMap.keySet()) {
            IModuleItemMeta metaData = metaDataMap.get(id);
            String className = metaData.getClassName();
            String configClassName = metaData.getConfigClassName();
            try {
                printLog("trying to load '" + className + "'");
                Class<?> cls = Class.forName(className);
                if(!IModuleItem.class.isAssignableFrom(cls)) {
                    printLog("Failed to load module '" + id + " : " + className + "' : invalid class type");
                    continue;
                }

                if(configClassName == null || configClassName.isEmpty()) {
                    configClassName = ModuleManager.ModuleItemConfig.class.getName();
                }
                printLog("trying to load '" + configClassName + "'");
                Class<?> configCls = Class.forName(configClassName);
				if(!IModuleItemConfig.class.isAssignableFrom(configCls)) {
					throw new ClassCastException("Failed to load module config '" + id + " : " + configClassName + "' : invalid config class type");
				}

                Class<? extends IModuleItem> moduleClass = (Class<? extends IModuleItem>) cls;
                Class<? extends IModuleItemConfig> configClass = (Class<? extends IModuleItemConfig>) configCls;
                ModuleManager.ModuleItemDescriptor descriptor = new ModuleManager.ModuleItemDescriptor(id, moduleClass, configClass);
                descriptors.put(id.toUpperCase(), descriptor);
                printLog("Module '" + id + " : " + className + " : " + configClassName + "' successfully initialized...");
            } catch(Exception exc) {
                printLog("Exception caught on createModuleItemDescriptor : " + exc);
                exc.printStackTrace();
            }
        }
        return descriptors;
    }

    /**
     * create configuration for specified module from json
     */
    public static List<IModuleItemConfig> createItemConfiguration(String json) {
        return createItemConfiguration(json, ModuleManager.getInstance().getItemDescriptors());
    }

    public static List<IModuleItemConfig> createItemConfiguration(String json, Map<String, IModuleItemDescriptor> descriptors) {
        printLog("createItemConfiguration : " + json + ", " + descriptors.size());
        List<IModuleItemConfig> itemConfigs = new ArrayList<>();
        if(json == null || json.isEmpty()) return itemConfigs;
        JsonArray arr = JsonParser.parseString(json).getAsJsonArray();
        for(JsonElement e : arr) {
            printLog("e : " + e.toString());
            JsonObject obj = e.getAsJsonObject();
            if(obj == null) {
				printLog("obj is null !!!");
                continue;
            }
			String id = null;
			if (obj.has("id")) {
				id = obj.get("id").getAsString().toUpperCase();
			} else if (obj.has("code")) {
				id = obj.get("code").getAsString().toUpperCase();
			}
			if (id == null) {
                printLog("id field not found !!!");
				continue;
			}
            boolean contain = descriptors.containsKey(id);
            if(!contain) {
                printLog("no config found for id '" + id + "'");
                continue;
            }

            IModuleItemDescriptor config = descriptors.get(id);
            try {
                IModuleItemConfig itemConfig = ModuleUtils.fromJson(e.toString(), config.getConfigClass());
                printLog("itemConfig : " + itemConfig);
				if (itemConfig == null) throw new NullPointerException("failed to create IModuleItemConfig<" + config.getConfigClass() + "> from " + e.toString());
                if(itemConfig.isActive()) itemConfigs.add(itemConfig);
            } catch(Exception exc) {
                exc.printStackTrace();
                printLog("Exception caught on createItemConfiguration : " + exc);
            }
        }

        return itemConfigs;
    }

    private static void printLog(String log) {
		System.out.println("UIMODULE -> ModuleFactory.java." + log);
	}

    //create default container to put the fragment
    public static FrameLayout createSectionContainer(IFragmentModule module) {
        FrameLayout fl = new FrameLayout(module.getContext());
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        fl.setLayoutParams(params);
        fl.setId(fl.hashCode());
        return fl;
    }

}
