package com.telkomsel.mytelkomsel.core.modules;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.lang.reflect.Type;

public class ModuleUtils {
    private ModuleUtils() { }

    //this func will parse json safely using setLenient true
    public static <T> T fromJson(String json, Type type) {
        try {
            Gson gson = new Gson();
            JsonReader reader = new JsonReader(new StringReader(json));
            reader.setLenient(true);
            return gson.fromJson(reader, type);
        } catch(Exception exc) {
            exc.printStackTrace();
			printLog("Exception caught on fromJson : " + exc.getMessage());
        }

        return null;
    }

    public static <T> T fromJson(String json, Type type, String defaultValue) {
        T result = fromJson(json, type);
        if (result == null) {
            result = fromJson(defaultValue, type);
        }
        return result;
    }

    public static <T> String toJson(T model) {
		try {
			return new Gson().toJson(model);
		} catch(Exception exc) {
			exc.printStackTrace();
			printLog("Exception caught on toJson : " + exc.getMessage());
		}

		return null;
	}

    public static String readFile(Context context, String fileName) {
        try {
            InputStream is = context.getAssets().open(fileName);
            return readFile(is);
        } catch(Exception exc) {
            exc.printStackTrace();
        }

        return null;
    }

    public static String readFile(Class cls, String fileName) {
        try {
            InputStream is = cls.getClassLoader().getResourceAsStream(fileName);
            return readFile(is);
        } catch(Exception exc) {
            exc.printStackTrace();
        }

        return null;
    }

    public static String readFile(File file) {
        printLog("readFile[isFile : " + file.isFile() + ", isDir : " + file.isDirectory() + ", exists : " + file.exists() + "]");
        if(!file.exists()) {
            printLog("not exists : " + file.getAbsolutePath());
            return null;
        }

        try {
            return readFile(new FileInputStream(file));
        } catch(Exception exc) {
            exc.printStackTrace();
            printLog("Exception caught on readFile.new FileInputStream[" + file.getAbsolutePath() + "] : " + exc.getMessage());
        }

        return null;
    }

    public static String readFile(InputStream inputStream) {
        try {
            String content = "";
            int bytesRead =-1;
            do {
				byte[] bytes = new byte[1024]; //clear buffer
                bytesRead = inputStream.read(bytes, 0, bytes.length);
				printLog("readFile[" + bytes.length + "] : " + bytesRead);
                if(bytesRead != -1) {
					byte[] realBytes = new byte[bytesRead];
					System.arraycopy(bytes, 0, realBytes, 0, realBytes.length);
                    String newData = new String(realBytes).trim();
                    content += newData;
					printLog("readFile[" + newData.length() + ":" + content.length() + "] : " + newData);
                }
            } while(bytesRead > -1);
			printLog("content[" + content.length() + "] : " + content.trim() + "#");
            inputStream.close();
            return content;
        } catch(Exception exc) {
            exc.printStackTrace();
			printLog("Exception caught on readFile : " + exc.getMessage());
        }

        return null;
    }

	private static void printLog(String log) { }

    public static void writeFile(String dir, String fileName, byte[] content) {
        printLog("writeFile[dir : " + dir + ", fileName : " + fileName + ", content : " + content + "]");
        File file = new File(dir, fileName);
        writeFile(file, content);
    }

    public static void writeFile(File file, byte[] content) {
        if(file == null) return;
        if(content == null) content = new byte[0];
        printLog("writeFile[isFile : " + file.isFile() + ", isDir : " + file.isDirectory() + ", exists : " + file.exists() + "]");
        if(file.isFile() && file.exists()) {
            printLog("delete : " + file.getAbsolutePath());
            file.delete();
        }

        try {
            File parentFile = file.getParentFile();
            if(parentFile != null) {
                printLog("parent[isFile : " + parentFile.isFile() + ", isDir : " + parentFile.isDirectory() + ", exists : " + parentFile.exists() + "] : " + parentFile.getAbsolutePath());
                if(!parentFile.exists() && !parentFile.mkdirs()) {
                    throw new Exception("failed creating directory hierarchy !!!");
                }
            }

            writeFile(new FileOutputStream(file), content);
        } catch(Exception exc) {
            exc.printStackTrace();
            printLog("Exception caught on writeFile.new FileOutputStream[" + file.getAbsolutePath() + "] : " + exc.getMessage());
        }
    }

    public static void writeFile(OutputStream outputStream, String content) {
        writeFile(outputStream, content.getBytes());
    }

    public static void writeFile(OutputStream outputStream, byte[] bytes) {
        try {
            if(outputStream == null) return;
            outputStream.write(bytes, 0, bytes.length);
            outputStream.close();
            printLog("writeFile success...");
        } catch(Exception exc) {
            exc.printStackTrace();
            printLog("Exception caught on writeFile[" + outputStream + ", " + bytes + "] : " + exc.getMessage());
        }
    }

    public static class Sorter implements java.util.Comparator<Sorter.IOrderable> {

        @Override
        public int compare(IOrderable o1, IOrderable o2) {
            if(o1.getOrder() == o2.getOrder()) return 0;
            return o1.getOrder() > o2.getOrder() ? 1 : -1;
        }

        public interface IOrderable {
            int getOrder();
        }
    }
}
