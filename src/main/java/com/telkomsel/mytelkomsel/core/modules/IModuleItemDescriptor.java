package com.telkomsel.mytelkomsel.core.modules;

/**
 * this interface is the concrete form of IModuleItemMeta interface
 * using this interface the real module item instance will be created
 */
public interface IModuleItemDescriptor {
    String getId();
    Class<? extends IModuleItem> getModuleClass();
    Class<? extends IModuleItemConfig> getConfigClass();
}

