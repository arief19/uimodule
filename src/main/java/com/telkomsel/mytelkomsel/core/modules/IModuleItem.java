package com.telkomsel.mytelkomsel.core.modules;

import android.content.Context;

public interface IModuleItem {
    IModuleItemConfig getConfig();

    /**
     * this is called when the instance is created
     * @param config
     */
    void initModule(IModuleItemConfig config, Context context);
}
