package com.telkomsel.mytelkomsel.core.modules;

public interface IModuleItemConfig extends ModuleUtils.Sorter.IOrderable {
    String getId();
	boolean isError();
	void setError(boolean error);
    boolean isActive();
}
