/*
 *	Created by ♠ on 2021/03/26 20:24:30
 *	Copyright (c) 2021 . All rights reserved.
 */

package com.telkomsel.mytelkomsel.core.modules;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.andropromise.Promise;
import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ModuleManager {

    private static ModuleManager _instance = null;
    public static ModuleManager getInstance(Context context, Map<String, IModuleItemMeta> metaDataMap) {
        if(_instance == null) {
            _instance = new ModuleManager(context, metaDataMap);
        }

        return _instance;
    }

    public static ModuleManager getInstance() {
        if(_instance == null) {
            throw new NullPointerException("initialize ModuleManager instance first by passing the context param");
        }

        return _instance;
    }

    private Context context = null;
    private List<OnLoadListener> onLoadListeners = null;
	private List<OnModuleItemRefreshedListener> onModuleItemRefreshedListeners = null;

    private Map<String, IModuleItemDescriptor> moduleItemDescriptors = null;
    private Map<IFragmentModule, List<IModuleItem>> moduleItems = null;
	private Map<IModuleItem, IModuleItemConfig> moduleItemConfigs = null;
    private List<IFragmentModule> initializingModules = null;
    private List<Class<?>> moduleClasses = null;
	private Map<Class<?>, Object> moduleInstances = null;

    //private constructor to prevent instance creation more than once
    private ModuleManager(Context context, Map<String, IModuleItemMeta> metaDataMap)
    {
        init(context, metaDataMap);
        if (context == null) {
            throw new NullPointerException("context is null");
        }

        onLoadListeners = new ArrayList<>();
		onModuleItemRefreshedListeners = new ArrayList<>();

        moduleItems = new HashMap<>();
		moduleItemConfigs = new HashMap<>();
        initializingModules = new ArrayList<>();
        moduleClasses = ModuleFactory.createModuleClasses(context);
		moduleInstances = new HashMap<>();
        printLog("instance created...");
    }

    private void init(Context context, Map<String, IModuleItemMeta> metaDataMap) {
        //do some initialization process
        if(this.context != null && this.context.equals(context.getApplicationContext())) return;
        this.context = context.getApplicationContext();
        printLog("ModuleManager.init : " + context);
        moduleItemDescriptors = ModuleFactory.createModuleItemDescriptor(metaDataMap);
    }

    public Class<?>[] getModuleClasses() {
		return moduleClasses.toArray(new Class<?>[0]);
	}

	public Object[] getModuleInstances() {
		return moduleInstances.values().toArray(new Object[0]);
	}

    public Map<String, IModuleItemDescriptor> getItemDescriptors() {
        return moduleItemDescriptors;
    }

    public void loadModule(IFragmentModule module) {
		if(module == null || module.getContainer() == null || module.getModuleFragmentManager() == null) return;
		boolean isInitializing = initializingModules.contains(module);
		printLog("loadModule.isInitializing[" + module + "] : " + isInitializing);
		if (isInitializing) return;
		initializingModules.add(module);
		module.initialize();
		List<IModuleItem> items = loadModuleItems(module);
		printLog("items : " + items.size());
		moduleItems.put(module, items);

		try {
			List<Promise> promises = new ArrayList<>();
			module.getContainer().removeAllViews();

            FragmentManager fm = module.getModuleFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.runOnCommit(() -> {
                printLog("onCommit !!!");
				fireOnModuleLoaded(module);
            });
			for (IModuleItem item : items) {
				// prevent duplicate fragment instance
				try {
					removeIfExists(item, fm, ft);
					promises.add(addView(item, module, ft));
				} catch(Exception ignored) { }
			}
			printLog("promises : " + promises.size());
			if(promises.size() > 0) {
				Promise.all(promises)
					.then(ret -> {
                        ft.commit();
						return ret;
					}).onCatch(t -> {
						fireOnModuleLoadFailed(module, new Exception(t));
                        ft.commit();
					}).start();
			} else {
				fireOnModuleLoaded(module);
			}
		} catch(Exception exc) {
			exc.printStackTrace();
			printLog("Exception caught on addView : " + exc);
			fireOnModuleLoadFailed(module, exc);
		}
    }

    private List<IModuleItem> loadModuleItems(IFragmentModule module) {
        Class<? extends IModuleItem> type = module.getModuleItemClass();
        printLog("loadModuleItems : " + type);
        List<IModuleItem> modules = new ArrayList<>();

        //sort configuration
        Collections.sort(module.getModuleItemConfiguration(), new ModuleUtils.Sorter());

        //create all module item instances
        for(IModuleItemConfig itemConfig : module.getModuleItemConfiguration()) {
            String id = itemConfig.getId().toUpperCase();
			printLog("initialize module : " + id);
            if(!moduleItemDescriptors.containsKey(id)) {
                printLog("No module configured for id '" + id + "'");
                continue;
            }

            try {
                IModuleItemDescriptor config = moduleItemDescriptors.get(id);
                Class<? extends IModuleItem> moduleClass = config.getModuleClass();
                //if this module item is derived from module item class specified
                printLog("creating new instance for : " + moduleClass);
                if(type.isAssignableFrom(moduleClass)) {
                    //create module item instance
                    IModuleItem item = moduleClass.newInstance();
					initModule(item, itemConfig, module.getContext());
                    modules.add(item);
                }
            } catch(Exception exc) {
                exc.printStackTrace();
                printLog("Exception caught on loadModules[" + type + "] : " + exc);
            }
        }
        return modules;
    }

	private void initModule(IModuleItem item, IModuleItemConfig config, Context context) {
		printLog("initModule : " + item + ", config : " + config);
		item.initModule(config, context);
		moduleItemConfigs.put(item, item.getConfig());
	}

    public List<IModuleItem> getModuleItems(IFragmentModule module) {
        if(moduleItems.containsKey(module)) {
            return moduleItems.get(module);
        }

        return null;
    }

    public List<IModuleItem> getModuleItems(IFragmentModule module, String... ids) {
        List<IModuleItem> items = getModuleItems(module);
        if(items == null) return null;
        List<String> idList = Arrays.asList(ids);
        List<IModuleItem> result = new ArrayList<>();
        for(IModuleItem item : items) {
			IModuleItemConfig config = item.getConfig();
			if(config == null) continue;
            String id = config.getId().toUpperCase();
            if(!idList.contains(id)) continue;
            result.add(item);
        }

        return result;
    }

	public IModuleItemConfig getModuleItemConfig(String id) {
		for (IModuleItemConfig config : moduleItemConfigs.values()) {
		    if (!config.getId().equalsIgnoreCase(id)) continue;
		    return config;
        }

		return null;
	}

	public IModuleItemConfig getModuleItemConfig(IModuleItem moduleItem) {
		if(moduleItemConfigs.containsKey(moduleItem)) {
			return moduleItemConfigs.get(moduleItem);
		}

		return null;
	}

    public ViewGroup getModuleItemContainer(IFragmentModule module, String id) {
        List<IModuleItem> items = getModuleItems(module, id);
        if(items == null) return null;
        for(IModuleItem item : items) {
            ViewGroup v = getContainer(item);
            if(v == null) continue;
            return v;
        }
        return null;
    }

    public List<ViewGroup> getModuleItemContainers(IFragmentModule module, String... ids) {
        List<IModuleItem> items = getModuleItems(module, ids);
        List<ViewGroup> views = new ArrayList<>();
        for(IModuleItem item : items) {
            ViewGroup v = getContainer(item);
            if(v == null) continue;
            views.add(v);
        }
        return views;
    }

	/**
	 * get Fragment class of this module
	 */
    public <T> T getOriginalModule(IFragmentModule module, String id, Class<T> type) {
        List<IModuleItem> items = getModuleItems(module, id);
        for(IModuleItem item : items) {
            if(type.isAssignableFrom(item.getClass())) {
                return (T) item;
            }
        }

        return null;
    }

	private void removeIfExists(IModuleItem item, FragmentManager fm, FragmentTransaction ft) {
		//remove existing IModuleItem
		printLog("removeIfExists : " + item +", config : " + item.getConfig());
		IModuleItemConfig config = item.getConfig();
		if (config == null) return;
		String id = config.getId().toUpperCase();
		Fragment f = fm.findFragmentByTag(id);
		boolean exists = (f != null);
		printLog("removeIfExists[" + id + "].exists : " + exists);
		if (exists) ft.remove(f);
	}

    private Promise addView(final IModuleItem item, IFragmentModule module, FragmentTransaction ft) {
        return new Promise(promiseResult -> {
			try {
				if (item == null) {
					promiseResult.reject(new Exception("item is NULL"));
					return;
				}

				View v = null;
				Fragment f = null;
				if (item instanceof View) {
					v = (View) item;
				} else if (item instanceof Fragment) {
					f = (Fragment) item;
				}

				IModuleItemConfig config = item.getConfig();
				if(config == null) throw new NullPointerException("IModuleItemConfig of '" + item + "' is NULL!!!");
				String tag = config.getId().toUpperCase();
				if (v != null) {
					v.setTag(tag);
					module.getContainer().addView(v);
					printLog("view added : " + v.getTag());
					promiseResult.resolve(v);
				} else if (f != null) {
					ViewGroup sectionContainer = ModuleFactory.createSectionContainer(module);
					sectionContainer.setTag(tag);
					module.getContainer().addView(sectionContainer);//add to recycler view later
					ft.replace(sectionContainer.getId(), f, tag);
					printLog("fragment added : " + f);
					promiseResult.resolve(f);
				} else {
					throw new Exception("IModuleItem is not a View nor Fragment. cannot inflate this item !!!");
				}
			} catch (Exception exc) {
				exc.printStackTrace();
				promiseResult.reject(exc);
			}
        });
    }

    public ViewGroup getContainer(IModuleItem item) {
        try {
            View v = null;
            Fragment f = null;
            if (item instanceof View) {
                v = (View) item;
            } else if (item instanceof Fragment) {
                f = (Fragment) item;
            }

            if (v != null) {
                return (ViewGroup) v;
            } else if (f != null && f.getView() != null) {
                return (ViewGroup) f.getView().getParent();
            }
        } catch(Exception exc) {
            exc.printStackTrace();
        }

        return null;
    }

    public <T> Class<?> tryGetModuleClass(Class<T> tClass) {
        try {
            List<Class<?>> classes = tryGetModuleClasses(tClass);
			if (classes == null || classes.size() <= 0) {
				throw new ClassNotFoundException("No module implemented for this '" + tClass + "' type");
			}
			return classes.get(0);
        } catch(Exception exc) {
            exc.printStackTrace();
            printLog("Exception caught on tryGetModuleClass : " + exc.getMessage());
        }

        return null;
	}

	public <T> List<Class<?>> tryGetModuleClasses(Class<T> tClass) {
		printLog("tryGetModuleClasses[" + tClass + "] : " + moduleClasses.size());
		List<Class<?>> classes = new ArrayList<Class<?>>();
		try {
			if (tClass == null) throw new NullPointerException("tClass may not NULL");

			for (Class<?> cls : moduleClasses) {
				boolean assignable = tClass.isAssignableFrom(cls);
				printLog("isAssignableFrom[" + cls + "] : " + assignable);
				if (!assignable) continue;
				classes.add(cls);
			}
		} catch(Exception exc) {
			exc.printStackTrace();
			printLog("Exception caught on tryGetModuleClasses : " + exc.getMessage());
		}

		return classes;
	}

	public <T> T tryGetModule(Class<T> tClass) {
	    printLog("tryGetModule[tClass : " + tClass + "].size : " + moduleInstances.size());
        try {
            List<T> modules = tryGetModules(tClass);
			if (modules == null || modules.size() <= 0) {
				throw new ClassNotFoundException("No module instance found for this '" + tClass + "' class");
			}
			return modules.get(0);
        } catch(Exception exc) {
            exc.printStackTrace();
            printLog("Exception caught on tryGetModule[tClass : " + tClass + "] : " + exc.getMessage());
        }

        return null;
	}

	public <T> List<T> tryGetModules(Class<T> tClass) {
	    printLog("tryGetModules[tClass : " + tClass + "].size : " + moduleInstances.size());
		List<T> modules = new ArrayList<T>();
		try {
			//get from loaded instances
			for(Class<?> key : moduleInstances.keySet().toArray(new Class<?>[0])) {
                Object instance = moduleInstances.get(key);
				boolean equals = tClass.equals(key);
                printLog("equals[" + key + ", " + instance + "] : " + equals);
				if(equals) { //found
					modules.add((T) instance);
				}
			}

			if(modules.size() <= 0) {
				/**
				 * not found on loaded instance
				 * try to load from loaded class (if found then create the instances immediately
				 */
				List<Class<?>> classes = tryGetModuleClasses(tClass); //exception will be thrown if class not found
				for (Class<?> cls : classes) {
					Object instance = cls.newInstance();
					printLog("creating new instance of class [" + cls + "].success : " + (instance != null));
					if(instance != null) {
						moduleInstances.put(tClass, instance);
						modules.add((T) instance);
					}
				}
			}
		} catch(Exception exc) {
			exc.printStackTrace();
			printLog("Exception caught on tryGetModule : " + exc.getMessage());
		}

		return modules;
	}

	//OnLoadListener
    public void addOnLoadListener(OnLoadListener listener) {
        printLog("addOnLoadListener : " + listener);
        if(onLoadListeners.contains(listener)) return;
        onLoadListeners.add(listener);
    }

    public void removeOnLoadListener(OnLoadListener listener) {
        printLog("removeOnLoadListener : " + listener);
        if(!onLoadListeners.contains(listener)) return;
        onLoadListeners.remove(listener);
    }

	/**
	 * add IModuleItem View to IModule container success
	 */
    private void fireOnModuleLoaded(IFragmentModule module) {
        printLog("fireOnModuleLoaded : " + module);
        for(OnLoadListener listener : onLoadListeners) {
            listener.onModuleLoaded(module);
        }
        initializingModules.remove(module);
    }

	/**
	 * add IModuleItem View to IModule container failed
	 */
    private void fireOnModuleLoadFailed(IFragmentModule module, Exception exc) {
        printLog("fireOnModuleLoadFailed : " + module + ", exc : " + exc.getMessage());
        for(OnLoadListener listener : onLoadListeners) {
            listener.onModuleLoadFailed(module, exc);
        }
        initializingModules.remove(module);
    }
	//============================

	//OnModuleItemRefreshedListener
	public void addOnModuleItemRefreshedListener(OnModuleItemRefreshedListener listener) {
		printLog("addOnModuleItemRefreshedListener : " + listener);
		if(onModuleItemRefreshedListeners.contains(listener)) return;
		onModuleItemRefreshedListeners.add(listener);
	}

	public void removeOnModuleItemRefreshedListener(OnModuleItemRefreshedListener listener) {
		printLog("removeOnModuleItemRefreshedListener : " + listener);
		if(!onModuleItemRefreshedListeners.contains(listener)) return;
		onModuleItemRefreshedListeners.remove(listener);
	}

	public void fireOnModuleItemRefreshed(IModuleItem moduleItem) {
		printLog("fireOnModuleItemRefreshed : " + moduleItem);
		for(OnModuleItemRefreshedListener listener : onModuleItemRefreshedListeners) {
			listener.onModuleItemRefreshed(moduleItem);
		}
	}
	//=============================

    private void printLog(String log) {
		System.out.println("UIMODULE -> ModuleManager.java." + log);
	}

    public interface OnLoadListener {
        void onModuleLoaded(IFragmentModule module);
        void onModuleLoadFailed(IFragmentModule module, Exception exc);
    }

	public interface OnModuleItemRefreshedListener {
		void onModuleItemRefreshed(IModuleItem moduleItem);
	}

    public static class ModuleItemMeta implements IModuleItemMeta {
        private String id = "";
        private String className = "";
        private String configClassName = "";

        public ModuleItemMeta(String id, String className, String configClassName) {
            this.id = id;
            this.className = className;
            this.configClassName = configClassName;
        }

        @Override
        public String getId() { return id; }

        @Override
        public String getClassName() { return className; }

        @Override
        public String getConfigClassName() { return configClassName; }
    }

    public static class ModuleItemDescriptor implements IModuleItemDescriptor {
        private String id = "";
        private Class<? extends IModuleItem> moduleClass = null;
        private Class<? extends IModuleItemConfig> configClass = null;

        public ModuleItemDescriptor(String id, Class<? extends IModuleItem> moduleClass, Class<? extends IModuleItemConfig> configClass) {
            this.id = id;
            this.moduleClass = moduleClass;
            this.configClass = configClass;
        }

        @Override
        public String getId() { return id; }
        @Override
        public Class<? extends IModuleItem> getModuleClass() { return moduleClass; }
        @Override
        public Class<? extends IModuleItemConfig> getConfigClass() { return configClass; }
    }

    public static class ModuleItemConfig implements IModuleItemConfig {
        @SerializedName(value = "id", alternate = "code")
        protected String id = "";
        @SerializedName("order")
        protected int order = 0;
		@SerializedName("isError")
		protected boolean error = false;
		@SerializedName("active")
		protected boolean isActive = true;

		public ModuleItemConfig() { }

        public ModuleItemConfig(String id, int order) {
            this.id = id;
            this.order = order;
        }

        @Override
        public String getId() { return id; }

        @Override
        public int getOrder() { return order; }

		@Override
		public boolean isError() { return error; }

		@Override
		public void setError(boolean error) { this.error = error; }

        @Override
        public boolean isActive() {
            return isActive;
        }

        public void setActive(boolean active) {
            isActive = active;
        }

        public void setCode(String id) {
            this.id = id;
        }

        @NonNull
        @Override
        public String toString() {
            return new Gson().toJson(this);
        }
    }

}
