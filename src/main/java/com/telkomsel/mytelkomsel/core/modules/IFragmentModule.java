package com.telkomsel.mytelkomsel.core.modules;

import android.content.Context;
import android.view.ViewGroup;

import androidx.fragment.app.FragmentManager;

import java.util.List;

/* this is used by the module container to provide all of the classes that related to this module */
public interface IFragmentModule {
    Context getContext();
    FragmentManager getModuleFragmentManager();
    ViewGroup getContainer();
    IModuleConfig getModuleConfiguration();
    List<IModuleItemConfig> getModuleItemConfiguration();
    //item type can be different for each IModule, as long as they are implement IModuleItem interface
    Class<? extends IModuleItem> getModuleItemClass();
    void initialize();
}
