package com.telkomsel.mytelkomsel.core.modules;

/**
 * this meta data contain a string description about the module item
 */
public interface IModuleItemMeta {
    String getId();
    String getClassName();
    String getConfigClassName();
}

